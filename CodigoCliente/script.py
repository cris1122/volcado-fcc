import os, csv, jmespath, json, sys, configparser, base64, requests

## Request Access Token
def request_token():
    client_auth = config['auth']['client_id'] + ":" + config['auth']['client_secret']

    req_headers = {
        'Authorization': 'Basic ' + base64.b64encode(client_auth.encode()).decode(),
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    req_body = {'grant_type': 'client_credentials'}
    response = requests.post('https://login.mypurecloud.com/token', data=req_body, headers=req_headers)
    
    if response.status_code != 200:
        sys.exit('Failure: ' + str(response.status_code) + ' - ' + response.reason)
    return response.json()

def obtener_codigo_cliente(conversationId, token):
    details = request_query_get('https://api.mypurecloud.com/api/v2/conversations/' + conversationId, token)
    clientCode = 'null'
    for x in details['participants']:
        if x['purpose'] == 'customer' and x['calls'][0]['direction'] == 'inbound':
            if x.get('attributes') and x['attributes'].get('ClientCode')!=None:
                clientCode = str(x['attributes']['ClientCode'])
                break
    
    return clientCode

def request_query_get(url=None, token=None):
    req_header = {
        'Authorization': token['token_type'] + ' ' + token['access_token']
    }

    if url == None:
        url = "https://api.mypurecloud.com/api/v2/routing/queues"

    response = requests.get(url, headers=req_header)

    if response.status_code == 404:
        return entrada
    else:
        if response.status_code == 500:
            sys.exit('Failure: ' + response.reason + ' - ' + str(response.json()))
        else:
            if response.status_code != 200:
                sys.exit('Failure: ' + str(response.status_code) + ' - ' + response.reason)

    return response.json()


config = configparser.ConfigParser()
config.read('config.ini')

## Define Directory
dir_name = config['input']['input_dir']
dir_name_output = config['output']['output_dir']

if os.path.isdir(dir_name) == False:
    os.mkdir(dir_name)

if os.path.isdir(dir_name_output) == False:
    os.mkdir(dir_name_output)

filename = dir_name + config['input']['file_entrada']
filename_salida = dir_name_output + config['output']['file_salida']

token=request_token()

with open(filename, 'r') as file:
    for line in file:
        clientCode = obtener_codigo_cliente(line.replace('\n', '').rstrip('\r\n'), token)

        with open(filename_salida, 'a') as file:
            writer = csv.writer(file, delimiter='|')
            writer.writerow([
                line.replace('\n', '').rstrip('\r\n'), clientCode
            ])

