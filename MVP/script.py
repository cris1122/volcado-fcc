import purecloud, os, csv, jmespath, json

config = purecloud.load_config()

## Define Directory
dir_name = config['output']['output_dir']

if os.path.isdir(dir_name) == False:
    os.mkdir(dir_name)

filename1 = dir_name + '/detallellamadaconversacion.csv'     
filename2 = dir_name + '/detallellamadaparticipant.csv'     
filename3 = dir_name + '/detallellamadasessions.csv'
filename4 = dir_name + '/detallellamadametrics.csv'
filename5 = dir_name + '/detallellamadasegment.csv'

purecloud.erasefile(filename1)
purecloud.erasefile(filename2)
purecloud.erasefile(filename3)
purecloud.erasefile(filename4)
purecloud.erasefile(filename5)

## DETALLELLAMADA ###
#query = purecloud.request_query_dtllamada()
indice = 1
respuesta = purecloud.request_query_dtllamada(indice)
purecloud.escribir_detallellamdaconversacion(respuesta, filename1)
purecloud.escribir_detallellamdaparticipants(respuesta, filename2)
purecloud.escribir_detallellamadasessions(respuesta, filename3)
purecloud.escribir_detallellamadametrics(respuesta, filename4)
purecloud.escribir_detallellamadasegments(respuesta, filename5)

while (len(respuesta) > 0):
    indice = indice +1
    respuesta = purecloud.request_query_dtllamada(indice)
    if(len(respuesta) > 0):
        ## Create Query Request - DETALLECONVERSACION
        purecloud.escribir_detallellamdaconversacion(respuesta, filename1)
        ## Create detallellamdaparticipants
        purecloud.escribir_detallellamdaparticipants(respuesta, filename2)
        # Create detallellamadasessions 
        purecloud.escribir_detallellamadasessions(respuesta, filename3)
        # Create detallellamadametrics
        purecloud.escribir_detallellamadametrics(respuesta, filename4)
        # Create detallellamadasegments
        purecloud.escribir_detallellamadasegments(respuesta, filename5)

print("File detallellamadaconversacion.csv ok!")
print("File detallellamadaparticipant.csv ok!")
print("File detallellamadasessions.csv ok!")
print("File detallellamadametrics.csv ok!")
print("File detallellamadasegment.csv ok!")

##
## SUMARIZADO COLAS ###
query = purecloud.request_query_sumarizado_colas()

with open(dir_name + '/sumaryqueue.csv', 'w') as file:
    ## Abres archivo
    writer = csv.writer(file, delimiter=';')

    items = jmespath.search("[].data[].{metrics:metrics, interval:interval, views:views}", query['results'])
    items1 = jmespath.search("[].group.{queueId:queueId, mediaType:mediaType}", query['results'])

    res = query["results"]
    num = range(len(res))

    #print(len(items1),len(items))
    nGranuralidad = config['query_queue']['granularity']

    for x in num:
        items = jmespath.search("["+str(x)+"].data[].{metrics:metrics,views:views,interval:interval}", query['results'])
        grupo = res[x]["group"]
        queueId = grupo["queueId"] if "queueId" in grupo else "00000000-0000-0000-0000-000000000000"
        mediaType = grupo["mediaType"] if "mediaType" in grupo else ""

        for data in items:
            nInterval = data['interval']
            nInterval = nInterval.split("/")
            nInterval_Inicio = nInterval[0]
            nInterval_Fin = nInterval[1]
            #split

            nError = purecloud.parse_metric('nError', data, {'count': 0})
            nOffered = purecloud.parse_metric('nOffered', data, {'count': 0})
            nOutbound = purecloud.parse_metric('nOutbound', data, {'count': 0})
            nOutboundAbandoned = purecloud.parse_metric('nOutboundAbandoned', data, {'count': 0})
            nOutboundAttemped = purecloud.parse_metric('nOutboundAttemped', data, {'count': 0})
            nOutboundConnected = purecloud.parse_metric('nOutboundconnected', data, {'count': 0})
            nOverSla = purecloud.parse_metric('nOverSla', data, {'count': 0})
            nStateTransitionError = purecloud.parse_metric('nStateTransitionError', data, {'count': 0})
            nTransferred = purecloud.parse_metric('nTransferred', data, {'count': 0})
            tAbandon = purecloud.parse_metric('tAbandon', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tAcd = purecloud.parse_metric('tAcd', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tAcw = purecloud.parse_metric('tAcw', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tAlert = purecloud.parse_metric('tAlert', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tAnswered = purecloud.parse_metric('tAnswered', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tFlowOut = purecloud.parse_metric('tFlowOut', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tHandle = purecloud.parse_metric('tHandle', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tHeld = purecloud.parse_metric('tHeld', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tHeldComplete = purecloud.parse_metric('tHeldComplete', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tIvr = purecloud.parse_metric('tIvr', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tNotRespondig = purecloud.parse_metric('tNotResponding', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tTalk = purecloud.parse_metric('tTalk', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tTalkComplete = purecloud.parse_metric('tTalkComplete', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tWait = purecloud.parse_metric('tWait', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})

            tAnswered10 = purecloud.parse_views('tAnswered10', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tAnswered15 = purecloud.parse_views('tAnswered15', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tAnswered20 = purecloud.parse_views('tAnswered20', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tAnswered30 = purecloud.parse_views('tAnswered30', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})

            tAbandon10 = purecloud.parse_views('tAbandon10', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tAbandon15 = purecloud.parse_views('tAbandon15', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tAbandon20 = purecloud.parse_views('tAbandon20', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
            tAbandon30 = purecloud.parse_views('tAbandon30', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})

            datum = str(tWait['sum']).split(".")
            datumMax = str(tWait['max']).split(".")
            datumMin = str(tWait['min']).split(".")

            writer.writerow([
                queueId,
                nGranuralidad,
                nInterval_Inicio,
                nInterval_Fin,
                mediaType,
                
                # error
                nError['count'],

                nOffered['count'],
                nOutbound['count'],
                nOutboundAbandoned['count'],
                nOutboundAttemped['count'],
                nOutboundConnected['count'],
                nOverSla['count'],
                nStateTransitionError['count'],
                nTransferred['count'],

                #tAbandon
                tAbandon['sum'],
                tAbandon['count'],
                tAbandon['min'],
                tAbandon['max'],

                #tAcd
                tAcd['sum'],
                tAcd['count'],
                tAcd['min'],
                tAcd['max'],

                #tAcw
                tAcw['sum'],
                tAcw['count'],
                tAcw['min'],
                tAcw['max'],

                #Talert
                tAlert['sum'],
                tAlert['count'],
                tAlert['min'],
                tAlert['max'],

                #tAnswered
                tAnswered['sum'],
                tAnswered['count'],
                tAnswered['min'],
                tAnswered['max'],

                #tFlowOut
                tFlowOut['sum'],
                tFlowOut['count'],
                tFlowOut['min'],
                tFlowOut['max'],

                #tHandle
                tHandle['sum'],
                tHandle['count'],
                tHandle['min'],
                tHandle['max'],

                #tHeld
                tHeld['sum'],
                tHeld['count'],
                tHeld['min'],
                tHeld['max'],

                #tHeldComplete
                tHeldComplete['sum'],
                tHeldComplete['count'],
                tHeldComplete['min'],
                tHeldComplete['max'],

                #tIvr
                tIvr['sum'],
                tIvr['count'],
                tIvr['min'],
                tIvr['max'],

                #tNotRespondig
                tNotRespondig['sum'],
                tNotRespondig['count'],
                tNotRespondig['min'],
                tNotRespondig['max'],

                #tTalk
                tTalk['sum'],
                tTalk['count'],
                tTalk['min'],
                tTalk['max'],

                #tTalkComplete
                tTalkComplete['sum'],
                tTalkComplete['count'],
                tTalkComplete['min'],
                tTalkComplete['max'],

                #tWait
                #tWait['sum'],
                datum[0],
                tWait['count'],
                #tWait['min'],
                datumMin[0],
                #tWait['max'],
                datumMax[0],

                #tAnswered10
                tAnswered10['sum'],
                tAnswered10['count'],
                tAnswered10['min'],
                tAnswered10['max'],

                #tAnswered15
                tAnswered15['sum'],
                tAnswered15['count'],
                tAnswered15['min'],
                tAnswered15['max'],

                #tAnswered20
                tAnswered20['sum'],
                tAnswered20['count'],
                tAnswered20['min'],
                tAnswered20['max'],

                #tAnswered30
                tAnswered30['sum'],
                tAnswered30['count'],
                tAnswered30['min'],
                tAnswered30['max'],

                #tAbandon10
                tAbandon10['sum'],
                tAbandon10['count'],
                tAbandon10['min'],
                tAbandon10['max'],

                #tAbandon15
                tAbandon15['sum'],
                tAbandon15['count'],
                tAbandon15['min'],
                tAbandon15['max'],
                
                #tAbandon20
                tAbandon20['sum'],
                tAbandon20['count'],
                tAbandon20['min'],
                tAbandon20['max'],

                #tAbandon30
                tAbandon30['sum'],
                tAbandon30['count'],
                tAbandon30['min'],
                tAbandon30['max']
            ])

    print("File sumaryqueue.csv ok!")
