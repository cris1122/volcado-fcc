import base64, requests, sys, configparser, json, jmespath, csv, os, datetime

config = configparser.ConfigParser()
config.read('config.ini')


## Load Script Config
def load_config():
    return config


## Debug JSON
def debug(obj):
    print(json.dumps(obj, indent=2))


## Request Access Token
def request_token():
    client_auth = config['auth']['client_id'] + ":" + config['auth']['client_secret']

    req_headers = {
        'Authorization': 'Basic ' + base64.b64encode(client_auth.encode()).decode(),
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    req_body = {'grant_type': 'client_credentials'}
    response = requests.post('https://login.mypurecloud.com/token', data=req_body, headers=req_headers)
    
    if response.status_code != 200:
        sys.exit('Failure: ' + str(response.status_code) + ' - ' + response.reason)
    return response.json()


## Request Query
def request_query(url=None, query=None):
    ## Obtener Token
    token = request_token()

    req_header = {
        'Authorization': token['token_type'] + ' ' + token['access_token']
    }

    if query == None:
        query = {
            "interval": obtener_fecha(),
            "order": config['query']['order'],
            "orderBy": config['query']['orderBy'],
            "paging": {
                "pageSize": config['query']['page_size'],
                "pageNumber": config['query']['page_number']
            }
        }

    if url == None:
        url = "https://api.mypurecloud.com/api/v2/analytics/conversations/details/query"

    response = requests.post(url, json=query, headers=req_header)

    if response.status_code == 500:
        sys.exit('Failure: ' + response.reason + ' - ' + str(response.json()))
    else:
        if response.status_code != 200:
            sys.exit('Failure: ' + str(response.status_code) + ' - ' + response.reason)

    return response.json()

## REQUEST QUERY - SUMARIZADO COLAS ####
def request_query_sumarizado_colas(query=None):
    url = "https://api.mypurecloud.com/api/v2/analytics/conversations/aggregates/query"
    body = {
        "interval": obtener_fecha(),
        "granularity": 'PT'+config['query_queue']['granularity']+'M',
        "groupBy": [
            "queueId"
        ],
        "filter": {
            "type": "or",
            "predicates": [
            {
                "type": "dimension",
                "dimension": "direction",
                "operator": "matches",
                "value": "inbound"
            }
            ]
        },
        "views": [
        {
            "name": "tAbandon10",
            "target": "tAbandon",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "10000"
            }
        },
        {
            "name": "tAbandon15",
            "target": "tAbandon",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "15000"
            }
        },
        {
            "name": "tAbandon20",
            "target": "tAbandon",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "20000"
            }
        },
        {
            "name": "tAbandon30",
            "target": "tAbandon",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "30000"
            }
        },
        {
            "name": "tAnswered10",
            "target": "tAnswered",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "10000"
            }
        },
        {
            "name": "tAnswered15",
            "target": "tAnswered",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "15000"
            }
        },
        {
        "name": "tAnswered20",
        "target": "tAnswered",
        "function": "rangeBound",
        "range": {
            "gte": 0,
            "lt": "20000"
        }
        },
        {
        "name": "tAnswered30",
        "target": "tAnswered",
        "function": "rangeBound",
        "range": {
            "gte": 0,
            "lt": "30000"
        }
        }
        ]
    }
    return request_query(url, body)

## REQUEST QUERY - DETALLE LLAMADA ###
#def request_query_dtllamada():
def request_query_dtllamada(pageNumber):
    url = "https://api.mypurecloud.com/api/v2/analytics/conversations/details/query"
    body = {
        "interval": obtener_fecha(),
        "order": config['query']['order'],
        "orderBy": config['query']['orderBy'],
        #"paging": {"pageSize": config['query']['page_size'], "pageNumber": config['query']['page_number']}
        "paging": {"pageSize": config['query']['page_size'], "pageNumber": pageNumber}
    }
    return request_query(url, body)

## FUNCICION PARA SUMARIZADO COLAS
def parse_metric(metric, arr, default):
    item = jmespath.search("metrics[?metric=='" + metric + "'].stats", arr)
    return item[0] if item else default

def parse_views(view, arr, default):
    item = jmespath.search("views[?name=='" + view + "'].stats", arr)
    return item[0] if item else default

#def parse_field(value):
#    valor = str(value).replace(',', '')
#    return valor

#Manejo de Ficheros, eliminar si existe previamente. Debe mejorar a dejar evidencia
def erasefile(filename):
    if os.path.exists(filename): os.remove(filename)
    return ''

#FUNCIONES PARA GRABAR CSV EN FICHEROS
def escribir_detallellamdaconversacion(query, filename):
    with open(filename, 'a') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter=';')

        for conv in query['conversations']:
            writer.writerow([
                conv.get('conversationId'),
                conv.get('conversationStart'),
                conv.get('conversationEnd')
            ])
    return ''

def escribir_detallellamdaparticipants(query, filename):
    with open(filename, 'a') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter=';')

        for conv in query['conversations']:
            for part in conv['participants']:
                #participantName = parse_field(part.get('participantName'))
                
                writer.writerow([
                    conv.get('conversationId'),
                    part.get('participantId'),
                    #participantName,
                    part.get('participantName'),
                    part.get('userId'),
                    part.get('purpose'),
                    part.get('externalContactId','null'),
                    part.get('externalOrganizationId','null'),
                    part.get('flaggedReason','null')
                ])
    return ''

def escribir_detallellamadasessions(query, filename):
    with open(filename, 'a') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter=';')

        for conv in query['conversations']:
            for part in conv['participants']:
                for sess in part['sessions']:
                    #remoteNameDisplayable = str(sess.get('remoteNameDisplayable')).replace(',', '')
                    writer.writerow([
                        part.get('participantId'),
                        sess.get('sessionId'),
                        sess.get('mediaType'),
                        sess.get('addressOther'),
                        sess.get('addressSelf'),
                        sess.get('addressFrom'),
                        sess.get('addressTo'),
                        sess.get('messageType'),
                        sess.get('ani'),
                        sess.get('direction'),
                        sess.get('dnis'),
                        sess.get('outboundCampaignId'),
                        sess.get('dispositionAnalyzer'),
                        sess.get('dispositionName'),
                        sess.get('edgeId'),
                        #remoteNameDisplayable,
                        sess.get('remoteNameDisplayable'),
                        sess.get('roomId'),
                        sess.get('monitoredsessionId'),
                        sess.get('monitoredparticipantId'),
                        sess.get('callbackuserName'),
                        sess.get('callbackNumbers'),
                        sess.get('callbackscheduledTime'),
                        sess.get('scriptId'),
                        sess.get('peerId'),
                        sess.get('sKipEnable'),
                        sess.get('timeoutSeconds'),
                        sess.get('cobrowseRole'),
                        sess.get('cobrowseroomId'),
                        sess.get('mediabridgeId'),
                        sess.get('screenshareaddressSelf'),
                        sess.get('sharingScreen'),
                        sess.get('screenshareroomId'),
                        sess.get('videoroomId'),
                        sess.get('videoaddressSelf')
                    ])

    return ''

def escribir_detallellamadametrics(query, filename):
    with open(filename, 'a') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter=';')

        for conv in query['conversations']:
            for part in conv['participants']:
                for sess in part['sessions']:
                    sessionId = sess.get('sessionId')
                    if sess.get('metrics'):
                        #met = sess.get('metrics')
                        for met in sess.get('metrics'):
                            metric_name = met['name']
                            metric_value = met['value']
                            metric_emitDate = met['emitDate']

                            writer.writerow([
                            sessionId,
                            metric_name,
                            metric_value,
                            metric_emitDate
                        ])
    return ''

def escribir_detallellamadasegments(query, filename):
    with open(filename, 'a') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter=';')

        for conv in query['conversations']:
            for part in conv['participants']:
                for sess in part['sessions']:
                    for segm in sess['segments']:
                        if segm.get('queueId'): 
                            queueId = segm.get('queueId') 
                        else: 
                            queueId = ''
                        
                        writer.writerow([
                            sess.get('sessionId'),
                            segm.get('segmentStart'),
                            segm.get('segmentEnd'),
                            segm.get('segmentType'),
                            queueId,
                            segm.get('disconnectType'),
                            segm.get('wrapUpCode'),
                            segm.get('wrapUpNote')
                        ])
    return ''


def obtener_fecha():
    today = datetime.datetime.today() 
    ahora = today.strftime('%Y-%m-%d')
    yesterday = (today + datetime.timedelta(days=-1))
    ayer = yesterday.strftime('%Y-%m-%d')
    interval=ayer+'T05:00:00.000Z/'+ahora+'T05:00:00.000Z'
    return interval