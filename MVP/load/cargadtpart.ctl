LOAD DATA
APPEND
INTO TABLE
CONCEN.TFCC003_PARTICIPANT
FIELDS TERMINATED BY ';' 
TRAILING NULLCOLS
(
CD_CONVERSACION char,
CD_PARTICIPANTE char,
NB_PARTICIPANTE char,
CD_USUARIO char,
NB_PROPOSITO char,
CD_CONTACTOEXT char,
CD_ORGANIZACIONEXT char,
CD_MOTMARCADO char
)
