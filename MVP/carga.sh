#!/bin/bash
echo "Carga masiva...."
#sqlldr="/usr/bin/sqlldr"
CONTROL="/output/load"
DATA="/output/data"
LOG="/output/log"
BAD="/output/bad"
ROWS="50000"

#/usr/lib/sqlldr gfccpe1/gfccpe1@118.180.35.145/tst12c CONTROL=/opt/purecloud/carga.ctl, DATA=/opt/purecloud/convfinal.txt ,LOG=/opt/purecloud/log1.log  ,BAD=/opt/purecloud/bad.log ,ROWS=5000000

#carga.ctl = tabla
#convfinal.txt = archivo CSV
#log1 = log del query

#DETELLELLAMADA
$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/cargadtconv.ctl DATA=$DATA/detallellamadaconversacion.csv LOG=$LOG/log_dtconversa.txt BAD=$BAD/bad_convfinal.log ERRORS=4000
$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/cargadtmt.ctl   DATA=$DATA/detallellamadametrics.csv      LOG=$LOG/log_dtmetrics.txt  BAD=$BAD/bad_metrica.log ERRORS=4000
$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/cargadtpart.ctl DATA=$DATA/detallellamadaparticipant.csv  LOG=$LOG/log_dtparti.txt    BAD=$BAD/bad_participante.log ERRORS=4000
$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/cargadtsegm.ctl DATA=$DATA/detallellamadasegment.csv  LOG=$LOG/log_segmt.txt BAD=$BAD/bad_segment.log ERRORS=4000
$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/cargadtsession.ctl DATA=$DATA/detallellamadasessions.csv  LOG=$LOG/log_session.txt BAD=$BAD/bad_session.log ERRORS=4000
$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/sumaryqueue.ctl DATA=$DATA/sumaryqueue.csv  LOG=$LOG/log_queue.txt BAD=$BAD/bad_queue.log ERRORS=4000 bindsize=20000000 readsize=20000000

#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@10.17.202.209:1521/ORCLPDB1 CONTROL=/home/cargadtconv.ctl DATA=/home/detallellamadaconversacion.csv LOG=/home/log_dtconversa.txt BAD=/home/bad_convfinal.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@10.17.202.209:1521/ORCLPDB1 CONTROL=/home/cargadtmt.ctl DATA=/home/detallellamadametrics.csv LOG=/home/log_dtmetrics.txt BAD=/home/bad_metrica.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@10.17.202.209:1521/ORCLPDB1 CONTROL=/home/cargadtpart.ctl DATA=/home/detallellamadaparticipant.csv LOG=/home/log_dtparti.txt BAD=/home/bad_participante.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@10.17.202.209:1521/ORCLPDB1 CONTROL=/home/cargadtsegm.ctl DATA=/home/detallellamadasegment.csv LOG=/home/log_segmt.txt BAD=/home/bad_segment.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@10.17.202.209:1521/ORCLPDB1 CONTROL=/home/cargadtsession.ctl DATA=/home/detallellamadasessions.csv LOG=/home/log_session.txt BAD=/home/bad_session.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@10.17.202.209:1521/ORCLPDB1 CONTROL=/home/sumaryqueue.ctl DATA=/home/sumaryqueue.csv LOG=/home/log_queue.txt BAD=/home/bad_queue.log ERRORS=4000


###
#$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/sumarypuesto.ctl DATA=$DATA/sumarypuestos.csv  LOG=$LOG/log_ BAD=$BAD/bad_puestos.log

#sqlldr CONCEN/password@10.17.202.209:1521/ORCLPDB1 CONTROL=cargadtconv.ctl DATA=detallellamadaconversacion.csv LOG=log_dtconversa.txt BAD=bad_convfinal.log ERRORS=4000
