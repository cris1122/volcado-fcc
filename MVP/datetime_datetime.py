import datetime

print('Now    :', datetime.datetime.now())
print('Today  :', datetime.datetime.today())
print('UTC Now:', datetime.datetime.utcnow())
print()

FIELDS = [
    'year', 'month', 'day',
    'hour', 'minute', 'second',
    'microsecond',
]

d = datetime.datetime.now()
for attr in FIELDS:
    print('{:15}: {}'.format(attr, getattr(d, attr)))

def obtener_fecha():
    today = datetime.datetime.today() 
    ahora = today.strftime('%Y-%m-%d')
    yesterday = (today + datetime.timedelta(days=-1))
    ayer = yesterday.strftime('%Y-%m-%d')
    interval=ayer+'T05:00:00.000Z/'+ahora+'T05:00:00.000Z'
    return interval

print(obtener_fecha())