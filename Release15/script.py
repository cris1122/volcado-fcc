import purecloud, os, csv, jmespath, json, sys, utilidades, datetime
from oracle import oracle

config = purecloud.load_config()

lista = []
if(len(sys.argv)>1):
    for arg in sys.argv:
        if(arg != ''):
            lista.append(arg)

if(len(lista) > 1):
    purecloud.intervalo = lista[1]+"/"+lista[2]
else:
    purecloud.intervalo = utilidades.obtener_intervalo()


## Define Directory
dir_name = config['output']['output_dir']

if os.path.isdir(dir_name) == False:
    os.mkdir(dir_name)

filename1 = dir_name + '/detallellamadaconversacion.csv'     
filename2 = dir_name + '/detallellamadaparticipant.csv'     
filename3 = dir_name + '/detallellamadasessions.csv'
filename4 = dir_name + '/detallellamadametrics.csv'
filename5 = dir_name + '/detallellamadasegment.csv'
filename6 = dir_name + '/catalogocolas.csv'
filename7 = dir_name + '/catalogousuarios.csv'

purecloud.erasefile(filename1)
purecloud.erasefile(filename2)
purecloud.erasefile(filename3)
purecloud.erasefile(filename4)
purecloud.erasefile(filename5)
purecloud.erasefile(filename6)

#Agent Status Sum  filter
if("query_agent_status" in config.sections() and config['query_agent_status'].get('filter_size_agent_status')):
    filterSizeAgentStatus = int(config['query_agent_status']['filter_size_agent_status'])
else:
    filterSizeAgentStatus = 50

## Oracle connection data
db_server = None
db_port = None
db_SID = None
db_user = None
db_password = None
db_schema = None
db_isServiceName = None


if("oracle" in config.sections()):
    db_server = config['oracle'].get('server')
    db_port = config['oracle'].get('port')
    db_SID = config['oracle'].get('sid')
    db_user = config['oracle'].get('user')
    db_password = config['oracle'].get('password')
    db_schema = config['oracle'].get('schema')
    db_isServiceName = config["oracle"].get("is_serviceName")
    if(db_isServiceName and str(db_isServiceName).lower() == "true"):
        db_isServiceName = True
    else:
        db_isServiceName = False

## DETALLELLAMADA ###
#query = purecloud.request_query_dtllamada()
indice = 1
token = purecloud.request_token()
respuesta = purecloud.request_query_dtllamada(indice, token)
if purecloud.is_not_empty(respuesta):
    purecloud.escribir_detallellamdaconversacion(respuesta, filename1)
    purecloud.escribir_detallellamdaparticipants(respuesta, filename2, token)
    purecloud.escribir_detallellamadasessions(respuesta, filename3)
    purecloud.escribir_detallellamadametrics(respuesta, filename4)
    purecloud.escribir_detallellamadasegments(respuesta, filename5, token)
    
    while (len(respuesta) > 0):
        indice = indice +1
        respuesta = purecloud.request_query_dtllamada(indice, token)
        if(len(respuesta) > 0):
            ## Create Query Request - DETALLECONVERSACION
            purecloud.escribir_detallellamdaconversacion(respuesta, filename1)
            ## Create detallellamdaparticipants
            purecloud.escribir_detallellamdaparticipants(respuesta, filename2, token)
            # Create detallellamadasessions 
            purecloud.escribir_detallellamadasessions(respuesta, filename3)
            # Create detallellamadametrics
            purecloud.escribir_detallellamadametrics(respuesta, filename4)
            # Create detallellamadasegments
            purecloud.escribir_detallellamadasegments(respuesta, filename5, token)

    print("File detallellamadaconversacion.csv ok!")
    print("File detallellamadaparticipant.csv ok!")
    print("File detallellamadasessions.csv ok!")
    print("File detallellamadametrics.csv ok!")
    print("File detallellamadasegment.csv ok!")

##
## SUMARIZADO COLAS ###
query = purecloud.request_query_sumarizado_colas(token)

if purecloud.is_not_empty(query):
    with open(dir_name + '/sumaryqueue.csv', 'w') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter='|')

        items = jmespath.search("[].data[].{metrics:metrics, interval:interval, views:views}", query['results'])
        items1 = jmespath.search("[].group.{queueId:queueId, mediaType:mediaType}", query['results'])

        res = query["results"]
        num = range(len(res))

        #print(len(items1),len(items))
        nGranuralidad = config['query_queue']['granularity']

        for x in num:
            items = jmespath.search("["+str(x)+"].data[].{metrics:metrics,views:views,interval:interval}", query['results'])
            grupo = res[x]["group"]
            queueId = grupo["queueId"] if "queueId" in grupo else "00000000-0000-0000-0000-000000000000"
            mediaType = grupo["mediaType"] if "mediaType" in grupo else ""

            for data in items:
                nInterval = data['interval']
                nInterval = nInterval.split("/")
                nInterval_Inicio = nInterval[0]
                nInterval_Fin = nInterval[1]
                #split

                nError = purecloud.parse_metric('nError', data, {'count': 0})
                nOffered = purecloud.parse_metric('nOffered', data, {'count': 0})
                nOutbound = purecloud.parse_metric('nOutbound', data, {'count': 0})
                nOutboundAbandoned = purecloud.parse_metric('nOutboundAbandoned', data, {'count': 0})
                nOutboundAttemped = purecloud.parse_metric('nOutboundAttemped', data, {'count': 0})
                nOutboundConnected = purecloud.parse_metric('nOutboundconnected', data, {'count': 0})
                nOverSla = purecloud.parse_metric('nOverSla', data, {'count': 0})
                nStateTransitionError = purecloud.parse_metric('nStateTransitionError', data, {'count': 0})
                nTransferred = purecloud.parse_metric('nTransferred', data, {'count': 0})
                #nBlindTransferred, nConsult, nConsultTransferred
                nBlindTransferred = purecloud.parse_metric('nBlindTransferred', data, {'count': 0})
                nConsult = purecloud.parse_metric('nConsult', data, {'count': 0})
                nConsultTransferred = purecloud.parse_metric('nConsultTransferred', data, {'count': 0})
                #fin
                tAbandon = purecloud.parse_metric('tAbandon', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAcd = purecloud.parse_metric('tAcd', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAcw = purecloud.parse_metric('tAcw', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAlert = purecloud.parse_metric('tAlert', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAnswered = purecloud.parse_metric('tAnswered', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tFlowOut = purecloud.parse_metric('tFlowOut', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tHandle = purecloud.parse_metric('tHandle', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tHeld = purecloud.parse_metric('tHeld', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tHeldComplete = purecloud.parse_metric('tHeldComplete', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tIvr = purecloud.parse_metric('tIvr', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tNotRespondig = purecloud.parse_metric('tNotResponding', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tTalk = purecloud.parse_metric('tTalk', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tTalkComplete = purecloud.parse_metric('tTalkComplete', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tWait = purecloud.parse_metric('tWait', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})

                tAnswered10 = purecloud.parse_views('tAnswered10', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAnswered15 = purecloud.parse_views('tAnswered15', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAnswered20 = purecloud.parse_views('tAnswered20', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAnswered30 = purecloud.parse_views('tAnswered30', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})

                tAbandon10 = purecloud.parse_views('tAbandon10', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAbandon15 = purecloud.parse_views('tAbandon15', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAbandon20 = purecloud.parse_views('tAbandon20', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAbandon30 = purecloud.parse_views('tAbandon30', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})

                datum = str(tWait['sum']).split(".")
                datumMax = str(tWait['max']).split(".")
                datumMin = str(tWait['min']).split(".")

                writer.writerow([
                    queueId,
                    nGranuralidad,
                    nInterval_Inicio,
                    nInterval_Fin,
                    mediaType,
                    
                    # error
                    nError['count'],

                    nOffered['count'],
                    nOutbound['count'],
                    nOutboundAbandoned['count'],
                    nOutboundAttemped['count'],
                    nOutboundConnected['count'],
                    nOverSla['count'],
                    nStateTransitionError['count'],
                    nTransferred['count'],
                    
                    #nBlindTransferred, nConsult, nConsultTransferred
                    nBlindTransferred['count'],
                    nConsult['count'],
                    nConsultTransferred['count'],
                    #fin

                    #tAbandon
                    tAbandon['sum'],
                    tAbandon['count'],
                    tAbandon['min'],
                    tAbandon['max'],

                    #tAcd
                    tAcd['sum'],
                    tAcd['count'],
                    tAcd['min'],
                    tAcd['max'],

                    #tAcw
                    tAcw['sum'],
                    tAcw['count'],
                    tAcw['min'],
                    tAcw['max'],

                    #Talert
                    tAlert['sum'],
                    tAlert['count'],
                    tAlert['min'],
                    tAlert['max'],

                    #tAnswered
                    tAnswered['sum'],
                    tAnswered['count'],
                    tAnswered['min'],
                    tAnswered['max'],

                    #tFlowOut
                    tFlowOut['sum'],
                    tFlowOut['count'],
                    tFlowOut['min'],
                    tFlowOut['max'],

                    #tHandle
                    tHandle['sum'],
                    tHandle['count'],
                    tHandle['min'],
                    tHandle['max'],

                    #tHeld
                    tHeld['sum'],
                    tHeld['count'],
                    tHeld['min'],
                    tHeld['max'],

                    #tHeldComplete
                    tHeldComplete['sum'],
                    tHeldComplete['count'],
                    tHeldComplete['min'],
                    tHeldComplete['max'],

                    #tIvr
                    tIvr['sum'],
                    tIvr['count'],
                    tIvr['min'],
                    tIvr['max'],

                    #tNotRespondig
                    tNotRespondig['sum'],
                    tNotRespondig['count'],
                    tNotRespondig['min'],
                    tNotRespondig['max'],

                    #tTalk
                    tTalk['sum'],
                    tTalk['count'],
                    tTalk['min'],
                    tTalk['max'],

                    #tTalkComplete
                    tTalkComplete['sum'],
                    tTalkComplete['count'],
                    tTalkComplete['min'],
                    tTalkComplete['max'],

                    #tWait
                    #tWait['sum'],
                    datum[0],
                    tWait['count'],
                    #tWait['min'],
                    datumMin[0],
                    #tWait['max'],
                    datumMax[0],

                    #tAnswered10
                    tAnswered10['sum'],
                    tAnswered10['count'],
                    tAnswered10['min'],
                    tAnswered10['max'],

                    #tAnswered15
                    tAnswered15['sum'],
                    tAnswered15['count'],
                    tAnswered15['min'],
                    tAnswered15['max'],

                    #tAnswered20
                    tAnswered20['sum'],
                    tAnswered20['count'],
                    tAnswered20['min'],
                    tAnswered20['max'],

                    #tAnswered30
                    tAnswered30['sum'],
                    tAnswered30['count'],
                    tAnswered30['min'],
                    tAnswered30['max'],

                    #tAbandon10
                    tAbandon10['sum'],
                    tAbandon10['count'],
                    tAbandon10['min'],
                    tAbandon10['max'],

                    #tAbandon15
                    tAbandon15['sum'],
                    tAbandon15['count'],
                    tAbandon15['min'],
                    tAbandon15['max'],
                    
                    #tAbandon20
                    tAbandon20['sum'],
                    tAbandon20['count'],
                    tAbandon20['min'],
                    tAbandon20['max'],

                    #tAbandon30
                    tAbandon30['sum'],
                    tAbandon30['count'],
                    tAbandon30['min'],
                    tAbandon30['max']
                ])

        print("File sumaryqueue.csv ok!")

##
## SUMARIZADO AGENTES ###
query = purecloud.request_query_sumarizado_puestos(token)

if purecloud.is_not_empty(query):
    with open(dir_name + '/sumarypuestos.csv', 'w') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter='|')

        items = jmespath.search("[].data[].{metrics:metrics, interval:interval}", query['results'])
        items1 = jmespath.search("[].group.{queueId:queueId, mediaType:mediaType}", query['results'])

        res = query["results"]
        num = range(len(res))

        #print(len(items1),len(items))
        nGranuralidad = config['query_queue']['granularity']

        for x in num:
            items = jmespath.search("["+str(x)+"].data[].{metrics:metrics,interval:interval}", query['results'])
            grupo = res[x]["group"]
            queueId = grupo["queueId"] if "queueId" in grupo else "00000000-0000-0000-0000-000000000000"
            userId = grupo["userId"] if "userId" in grupo else "00000000-0000-0000-0000-000000000000"
            mediaType = grupo["mediaType"] if "mediaType" in grupo else ""

            for data in items:
                nInterval = data['interval']
                nInterval = nInterval.split("/")
                nInterval_Inicio = nInterval[0]
                nInterval_Fin = nInterval[1]
                #split

                nError = purecloud.parse_metric('nError', data, {'count': 0})
                nOffered = purecloud.parse_metric('nOffered', data, {'count': 0})
                nOutbound = purecloud.parse_metric('nOutbound', data, {'count': 0})
                nOutboundAbandoned = purecloud.parse_metric('nOutboundAbandoned', data, {'count': 0})
                nOutboundAttemped = purecloud.parse_metric('nOutboundAttemped', data, {'count': 0})
                nOutboundConnected = purecloud.parse_metric('nOutboundconnected', data, {'count': 0})
                nOverSla = purecloud.parse_metric('nOverSla', data, {'count': 0})
                nStateTransitionError = purecloud.parse_metric('nStateTransitionError', data, {'count': 0})
                nTransferred = purecloud.parse_metric('nTransferred', data, {'count': 0})
                tAbandon = purecloud.parse_metric('tAbandon', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAcd = purecloud.parse_metric('tAcd', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAcw = purecloud.parse_metric('tAcw', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAlert = purecloud.parse_metric('tAlert', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tAnswered = purecloud.parse_metric('tAnswered', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tFlowOut = purecloud.parse_metric('tFlowOut', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tHandle = purecloud.parse_metric('tHandle', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tHeld = purecloud.parse_metric('tHeld', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tHeldComplete = purecloud.parse_metric('tHeldComplete', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tIvr = purecloud.parse_metric('tIvr', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tNotRespondig = purecloud.parse_metric('tNotResponding', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tTalk = purecloud.parse_metric('tTalk', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tTalkComplete = purecloud.parse_metric('tTalkComplete', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})
                tWait = purecloud.parse_metric('tWait', data, {'max': 0, 'min': 0, 'count': 0, 'sum': 0})

                datum = str(tWait['sum']).split(".")
                datumMax = str(tWait['max']).split(".")
                datumMin = str(tWait['min']).split(".")

                writer.writerow([
                    nInterval_Inicio,
                    nInterval_Fin,
                    nGranuralidad,
                    mediaType,
                    queueId,
                    userId,

                    # error
                    nError['count'],

                    nOffered['count'],
                    nOutbound['count'],
                    nOutboundAbandoned['count'],
                    nOutboundAttemped['count'],
                    nOutboundConnected['count'],
                    nOverSla['count'],
                    nStateTransitionError['count'],
                    nTransferred['count'],

                    #tAbandon
                    tAbandon['sum'],
                    tAbandon['count'],
                    tAbandon['min'],
                    tAbandon['max'],

                    #tAcd
                    tAcd['sum'],
                    tAcd['count'],
                    tAcd['min'],
                    tAcd['max'],

                    #tAcw
                    tAcw['sum'],
                    tAcw['count'],
                    tAcw['min'],
                    tAcw['max'],

                    #Talert
                    tAlert['sum'],
                    tAlert['count'],
                    tAlert['min'],
                    tAlert['max'],

                    #tAnswered
                    tAnswered['sum'],
                    tAnswered['count'],
                    tAnswered['min'],
                    tAnswered['max'],

                    #tFlowOut
                    tFlowOut['sum'],
                    tFlowOut['count'],
                    tFlowOut['min'],
                    tFlowOut['max'],

                    #tHandle
                    tHandle['sum'],
                    tHandle['count'],
                    tHandle['min'],
                    tHandle['max'],

                    #tHeld
                    tHeld['sum'],
                    tHeld['count'],
                    tHeld['min'],
                    tHeld['max'],

                    #tHeldComplete
                    tHeldComplete['sum'],
                    tHeldComplete['count'],
                    tHeldComplete['min'],
                    tHeldComplete['max'],

                    #tIvr
                    tIvr['sum'],
                    tIvr['count'],
                    tIvr['min'],
                    tIvr['max'],

                    #tNotRespondig
                    tNotRespondig['sum'],
                    tNotRespondig['count'],
                    tNotRespondig['min'],
                    tNotRespondig['max'],

                    #tTalk
                    tTalk['sum'],
                    tTalk['count'],
                    tTalk['min'],
                    tTalk['max'],

                    #tTalkComplete
                    tTalkComplete['sum'],
                    tTalkComplete['count'],
                    tTalkComplete['min'],
                    tTalkComplete['max'],

                    #tWait
                    #tWait['sum'],
                    datum[0],
                    tWait['count'],
                    #tWait['min'],
                    datumMin[0],
                    #tWait['max'],
                    datumMax[0]
                ])

        print("File sumarypuestos.csv ok!")

##
## CATALOGO COLAS ###
indc = 0
query = purecloud.request_query_cola(indc, token, {} )

if purecloud.is_not_empty(query):
    purecloud.escribir_catalogocolas(query, filename6)
    if ( query['pageCount'] > 1 ):
        while (len(query['entities']) > 0):
            indc = indc +1
            query = purecloud.request_query_cola(indc,token)
            if(len(query['entities']) > 0):
                purecloud.escribir_catalogocolas(query, filename6)
            
    print("File catalogocolas.csv ok!")

#Arreglo para guardar todos los ids de los usuarios de Purecloud
users_id = []


##
## CATALOGO USUARIOS ###
indc = 1
query = purecloud.request_query_usuarios(indc, token, {})

if purecloud.is_not_empty(query):    
    users_id.extend(jmespath.search("[].id", query['entities']))
    purecloud.escribir_catalogousuarios(query, filename7)
    if ( query['pageCount'] > 1 ):
        while (len(query['entities']) > 0):
            indc = indc +1
            query = purecloud.request_query_usuarios(indc, token)
            if(len(query['entities']) > 0):
                users_id.extend(jmespath.search("[].id", query['entities']))
                purecloud.escribir_catalogousuarios(query, filename7)
            
    print("File catalogousuarios.csv ok!")

##
## SUMARIZADO ESTATUS AGENTE ###
resultAgentStatus = []
totalAgentStatusSum = 0
oracleObject = None

print("Inicia sumarizado estatus de agente " + str(datetime.datetime.now()))
try:

    print("Obteniendo datos de API " + str(datetime.datetime.now()))
    for index in range(0, len(users_id), filterSizeAgentStatus):        
        resultAgentStatus.append(purecloud.request_query_sumarizado_estatus_agente(token, users_id[index:index + filterSizeAgentStatus]))

    print("Termina de obtener datos de API " + str(datetime.datetime.now()))

    print("Inicia inserción de BD " + str(datetime.datetime.now()))
    oracleObject = oracle(db_server, db_port, db_SID, db_user, db_password, db_schema, db_isServiceName )    
    totalAgentStatusSum = oracleObject.saveAgentStatusSum(resultAgentStatus)
    print("Termina inserción de BD " + str(datetime.datetime.now()))    
except Exception as ex:
    print("Error al guardar la informacion de agregado estatus de agente. {}".format(str(ex)))
finally:
    if(oracleObject  and oracleObject.connection):
        oracleObject.connection.close()  
    
print("Termina sumarizado estatus agente - total: {} {}".format(str(totalAgentStatusSum), str(datetime.datetime.now())))


##
## CATALOGO SUPERVISOR AGENTE ###
#grupo Supervisor
indc = 1
query = purecloud.request_query_grupo_supervisor(indc, token, {})

if purecloud.is_not_empty(query):
    groupId = purecloud.buscar_grupo_supervisor(query) 
    if ( query['pageCount'] > 1 and groupId == ''):
        while (len(query['entities']) > 0):
            indc = indc +1
            query = purecloud.request_query_grupo_supervisor(indc)
            groupId = purecloud.buscar_grupo_supervisor(query)

#Supervisores
indc = 1
query = purecloud.request_query_supervisores(groupId, indc, token, {})

if purecloud.is_not_empty(query):
    lista = purecloud.buscar_supervisor(query)

    if ( query['pageCount'] > 1 ):
        while (len(query['entities']) > 0):
            indc = indc +1
            query = purecloud.request_query_supervisores(groupId, indc, token, {})
            lista2 = purecloud.buscar_supervisor(query)
            lista.extend(lista2)

    #agentes por supervisor
    for item in lista:
        query = purecloud.request_query_agente_supervisor(item, token, {})
        num = range(len(query))
        for x in num:
            with open(dir_name + '/supervisoragente.csv', 'a') as file:
                ## Abres archivo
                writer = csv.writer(file, delimiter='|')
                writer.writerow([
                    query[x]['id'],
                    item        
                ])

    print("File supervisoragente.csv ok!")