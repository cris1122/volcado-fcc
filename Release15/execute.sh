#!/bin/bash
#export https_proxy=http://118.180.54.170:8080/
#export http_proxy=http://118.180.54.170:8080/

declare -a array
#python script.py
#rm -rf /output/data/*csv
rm -rf /Users/jdluna/channels/contact-center/output/prueba/*csv

a=0
for i in "$@"
do
    array[$a]=$i
    a=$((a+1))
done

# get length of an array
arraylength=${#array[@]}

#echo "size :  ${arraylength} "

if [ ${arraylength} -gt 0 ]
then
    echo "Download appi data Purecloud..."
    #/opt/rh/rh-python36/root/bin/python3.6 script.py
    #python3 script.py
    nombre=${array[0]}_${array[1]}
    python3 script.py ${array[0]} ${array[1]}    
    ret_value=$?
else
    echo "Download appi data Purecloud..."
    #/opt/rh/rh-python36/root/bin/python3.6 script.py
    python3 script.py
    ret_value=$?
    nombre=$(python3 -c "import utilidades; utilidades.obtener_intervalo()")
    nombre="${nombre//\//_}"
fi

if [ $ret_value -eq 0 ]
then
    echo "Upload data Oracle"
    #sh carga.sh
    mkdir -p /home/userbbva/Documentos/Reporteria/Volcado/Output/$nombre
    cp -v /home/userbbva/Documentos/Reporteria/Volcado/Output/*csv /home/userbbva/Documentos/Reporteria/Volcado/Output/$nombre
fi

rm -rf __pycache__
echo "Fin..."
