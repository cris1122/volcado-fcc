import cx_Oracle, sys, time
import re
import datetime


class oracle:

    # Constructor
    def __init__(self, server, port, SID, user, password, schema, isServiceName):
        self.server = server
        self.port = port
        self.SID = SID
        self.user = user
        self.password = password
        self.isServiceName = isServiceName
        if(schema):
            self.schema = schema + "."
        else:
            self.schema = ""        
        self.connection = self.connect()



    def connect(self, maxAttempts = 3, timeSleep = 5):
        attempt = 0      
        
        #connect to database
        dsn_tns = cx_Oracle.makedsn(self.server, self.port, self.SID)

        if(self.isServiceName):
            dsn_tns = dsn_tns.replace('SID','SERVICE_NAME')
        
       
        while(attempt < maxAttempts):
            try:
                attempt += 1
                return cx_Oracle.connect(self.user, self.password, dsn_tns)                
            except Exception as ex:
                print("Oracle error: " + str(ex) + " attempt: " + str(attempt))
                if(attempt == maxAttempts):
                    raise ex
                time.sleep(timeSleep) #wait 5 seconds


    def getStatusName(self, id, mappings):
        if(mappings):
            for status in mappings:
                if(id in mappings[status]):
                    return status
        return None


    def saveAgentStatusSum(self, resultados):
        
        regexInterval = r'\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d+Z'
        regexGuid = r'[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}'
        userId = None
        interval = []
        startInterval = None
        endInterval = None
        metric = None
        value = 0
        qualifierId = None
        qualifier = None
        total = 0

        #funcion lambda para obtener el status del agente cuando viene el id
        

        cursor = self.connection.cursor()            
        self.connection.begin()
        
        for r in resultados:
            if(r.get("results")):                
                for item in r["results"]:
                    userId = None
                    if(item.get("group")):
                        userId = item.get("group").get("userId")
                if(item.get("data")):                    
                    for data in item["data"]:
                        startInterval = None
                        endInterval = None                        
                        interval = data.get("interval").split("/")
                        if(len(interval) == 2):
                            if(re.match(regexInterval,interval[0])):
                                startInterval = datetime.datetime.strptime(interval[0], '%Y-%m-%dT%H:%M:%S.%fZ')
                            if(re.match(regexInterval,interval[1])):
                                endInterval = datetime.datetime.strptime(interval[1], '%Y-%m-%dT%H:%M:%S.%fZ')

                        for me in data["metrics"]:
                            metric = None
                            qualifierId = None
                            qualifier = None
                            value = 0
                            metric = me.get("metric")
                            qualifierId = me.get("qualifier")
                            if(re.match(regexGuid,qualifierId)):
                                qualifier = self.getStatusName(qualifierId, r.get("systemToOrganizationMappings"))                           
                            if(me.get("stats")):
                                value = me.get("stats").get("sum")
                            
                            try:
                                if(userId and startInterval and endInterval and metric and value and qualifierId):
                                    cursor.callproc(self.schema + "SPI_TFCC043_ESTATUSUASUM", [userId, startInterval, endInterval, metric, value, qualifierId, qualifier])
                                    total += 1
                            except Exception as ex:
                                self.connection.rollback()                                 
                                raise ex                                          
                                                            
 
        self.connection.commit()        
        cursor.close()
        return total
              
      
        


