#!/bin/bash
echo "Carga masiva...."
#sqlldr="/usr/bin/sqlldr"
CONTROL="/output/load"
DATA="/Users/jdluna/channels/contact-center/output/prueba"
LOG="/output/log"
BAD="/output/bad"
ROWS="50000"

#/usr/lib/sqlldr gfccpe1/gfccpe1@118.180.35.145/tst12c CONTROL=/opt/purecloud/carga.ctl, DATA=/opt/purecloud/convfinal.txt ,LOG=/opt/purecloud/log1.log  ,BAD=/opt/purecloud/bad.log ,ROWS=5000000

#carga.ctl = tabla
#convfinal.txt = archivo CSV
#log1 = log del query

#DETELLELLAMADA
if [ -e "$DATA/detallellamadaconversacion.csv" ]
then
    sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/cargadtconv.ctl DATA=$DATA/detallellamadaconversacion.csv LOG=$LOG/log_dtconversa.txt BAD=$BAD/bad_convfinal.log ERRORS=90000
else
    echo "$0: File '$DATA/detallellamadaconversacion.csv' not found."
fi

if [ -e "$DATA/detallellamadametrics.csv" ]
then
    sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/cargadtmt.ctl DATA=$DATA/detallellamadametrics.csv LOG=$LOG/log_dtmetrics.txt  BAD=$BAD/bad_metrica.log ERRORS=90000
else
    echo "$0: File '$DATA/detallellamadametrics.csv' not found."
fi

if [ -e "$DATA/detallellamadaparticipant.csv" ]
then
    sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/cargadtpart.ctl DATA=$DATA/detallellamadaparticipant.csv  LOG=$LOG/log_dtparti.txt    BAD=$BAD/bad_participante.log ERRORS=90000
else
    echo "$0: File '$DATA/detallellamadaparticipant.csv' not found."
fi

if [ -e "$DATA/detallellamadasegment.csv" ]
then
    sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/cargadtsegm.ctl DATA=$DATA/detallellamadasegment.csv LOG=$LOG/log_segmt.txt BAD=$BAD/bad_segment.log ERRORS=90000 bindsize=20000000 readsize=20000000
else
    echo "$0: File '$DATA/detallellamadasegment.csv' not found."
fi

if [ -e "$DATA/detallellamadasessions.csv" ]
then
    sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/cargadtsession.ctl DATA=$DATA/detallellamadasessions.csv  LOG=$LOG/log_session.txt BAD=$BAD/bad_session.log ERRORS=90000 bindsize=20000000 readsize=20000000
else
    echo "$0: File '$DATA/detallellamadasessions.csv' not found."
fi

if [ -e "$DATA/sumaryqueue.csv" ]
then
    sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/sumaryqueue.ctl DATA=$DATA/sumaryqueue.csv  LOG=$LOG/log_queue.txt BAD=$BAD/bad_queue.log ERRORS=90000 bindsize=20000000 readsize=20000000
else
    echo "$0: File '$DATA/sumaryqueue.csv' not found."
fi

if [ -e "$DATA/sumarypuestos.csv" ]
then
    sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/sumarypuestos.ctl DATA=$DATA/sumarypuestos.csv  LOG=$LOG/log_puestos.txt BAD=$BAD/bad_puestos.log ERRORS=90000 bindsize=20000000 readsize=20000000
else
    echo "$0: File '$DATA/sumarypuestos.csv' not found."
fi

if [ -e "$DATA/catalogocolas.csv" ]
then
    sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/catalogocolas.ctl DATA=$DATA/catalogocolas.csv  LOG=$LOG/log_catalogocolas.txt BAD=$BAD/bad_catalogocolas.log ERRORS=90000 bindsize=20000000 readsize=20000000
else
    echo "$0: File '$DATA/catalogocolas.csv' not found."
fi

if [ -e "$DATA/catalogousuarios.csv" ]
then
    sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/catalogousuarios.ctl DATA=$DATA/catalogousuarios.csv  LOG=$LOG/log_catalogousuarios.txt BAD=$BAD/bad_catalogousuarios.log ERRORS=90000 bindsize=20000000 readsize=20000000
else
    echo "$0: File '$DATA/catalogousuarios.csv' not found."
fi

if [ -e "$DATA/supervisoragente.csv" ]
then
    sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/supervisoragente.ctl DATA=$DATA/supervisoragente.csv  LOG=$LOG/log_supervisoragente.txt BAD=$BAD/bad_supervisoragente.log ERRORS=90000 bindsize=20000000 readsize=20000000
else
    echo "$0: File '$DATA/supervisoragente.csv' not found."
fi


#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@localhost.oracle:1521/ORCLPDB1 CONTROL=/home/cargadtconv.ctl DATA=/home/detallellamadaconversacion.csv LOG=/home/log_dtconversa.txt BAD=/home/bad_convfinal.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@localhost.oracle:1521/ORCLPDB1 CONTROL=/home/cargadtmt.ctl DATA=/home/detallellamadametrics.csv LOG=/home/log_dtmetrics.txt BAD=/home/bad_metrica.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@localhost.oracle:1521/ORCLPDB1 CONTROL=/home/cargadtpart.ctl DATA=/home/detallellamadaparticipant.csv LOG=/home/log_dtparti.txt BAD=/home/bad_participante.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@localhost.oracle:1521/ORCLPDB1 CONTROL=/home/cargadtsegm.ctl DATA=/home/detallellamadasegment.csv LOG=/home/log_segmt.txt BAD=/home/bad_segment.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@localhost.oracle:1521/ORCLPDB1 CONTROL=/home/cargadtsession.ctl DATA=/home/detallellamadasessions.csv LOG=/home/log_session.txt BAD=/home/bad_session.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@localhost.oracle:1521/ORCLPDB1 CONTROL=/home/sumaryqueue.ctl DATA=/home/sumaryqueue.csv LOG=/home/log_queue.txt BAD=/home/bad_queue.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@localhost.oracle:1521/ORCLPDB1 CONTROL=/home/sumarypuestos.ctl DATA=/home/sumarypuestos.csv LOG=/home/log_puestos.txt BAD=/home/bad_puestos.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@localhost.oracle:1521/ORCLPDB1 CONTROL=/home/catalogocolas.ctl DATA=/home/catalogocolas.csv LOG=/home/log_catalogocolas.txt BAD=/home/bad_catalogocolas.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@localhost.oracle:1521/ORCLPDB1 CONTROL=/home/catalogousuarios.ctl DATA=/home/catalogousuarios.csv LOG=/home/log_catalogousuarios.txt BAD=/home/bad_catalogousuarios.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@localhost.oracle:1521/ORCLPDB1 CONTROL=/home/supervisoragente.ctl DATA=/home/supervisoragente.csv LOG=/home/log_supervisoragente.txt BAD=/home/bad_supervisoragente.log ERRORS=4000

#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@192.168.1.3:1521/ORCLPDB1 CONTROL=/home/cargadtconv.ctl DATA=/home/detallellamadaconversacion.csv LOG=/home/log_dtconversa.txt BAD=/home/bad_convfinal.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@192.168.1.3:1521/ORCLPDB1 CONTROL=/home/cargadtmt.ctl DATA=/home/detallellamadametrics.csv LOG=/home/log_dtmetrics.txt BAD=/home/bad_metrica.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@192.168.1.3:1521/ORCLPDB1 CONTROL=/home/cargadtpart.ctl DATA=/home/detallellamadaparticipant.csv LOG=/home/log_dtparti.txt BAD=/home/bad_participante.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@192.168.1.3:1521/ORCLPDB1 CONTROL=/home/cargadtsegm.ctl DATA=/home/detallellamadasegment.csv LOG=/home/log_segmt.txt BAD=/home/bad_segment.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@192.168.1.3:1521/ORCLPDB1 CONTROL=/home/cargadtsession.ctl DATA=/home/detallellamadasessions.csv LOG=/home/log_session.txt BAD=/home/bad_session.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@192.168.1.3:1521/ORCLPDB1 CONTROL=/home/sumaryqueue.ctl DATA=/home/sumaryqueue.csv LOG=/home/log_queue.txt BAD=/home/bad_queue.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@192.168.1.3:1521/ORCLPDB1 CONTROL=/home/sumarypuestos.ctl DATA=/home/sumarypuestos.csv LOG=/home/log_puestos.txt BAD=/home/bad_puestos.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@192.168.1.3:1521/ORCLPDB1 CONTROL=/home/catalogocolas.ctl DATA=/home/catalogocolas.csv LOG=/home/log_catalogocolas.txt BAD=/home/bad_catalogocolas.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@192.168.1.3:1521/ORCLPDB1 CONTROL=/home/catalogousuarios.ctl DATA=/home/catalogousuarios.csv LOG=/home/log_catalogousuarios.txt BAD=/home/bad_catalogousuarios.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@192.168.1.3:1521/ORCLPDB1 CONTROL=/home/supervisoragente.ctl DATA=/home/supervisoragente.csv LOG=/home/log_supervisoragente.txt BAD=/home/bad_supervisoragente.log ERRORS=4000


###
#$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/sumarypuesto.ctl DATA=$DATA/sumarypuestos.csv  LOG=$LOG/log_ BAD=$BAD/bad_puestos.log

#sqlldr CONCEN/password@10.17.202.209:1521/ORCLPDB1 CONTROL=cargadtconv.ctl DATA=detallellamadaconversacion.csv LOG=log_dtconversa.txt BAD=bad_convfinal.log ERRORS=4000
