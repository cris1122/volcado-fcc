import base64, requests, sys, configparser, json, jmespath, csv, os, datetime, utilidades, time

config = configparser.ConfigParser()
config.read('config.ini')

#max retries reached when attemping an API call
maxAttempts = 5

#intervalo = utilidades.obtener_intervalo()

## Load Script Config
def load_config():
    return config


## Debug JSON
def debug(obj):
    print(json.dumps(obj, indent=2))


## Request Access Token
def request_token():
    client_auth = config['auth']['client_id'] + ":" + config['auth']['client_secret']

    req_headers = {
        'Authorization': 'Basic ' + base64.b64encode(client_auth.encode()).decode(),
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    req_body = {'grant_type': 'client_credentials'}
    response = requests.post('https://login.mypurecloud.com/token', data=req_body, headers=req_headers)
    
    if response.status_code != 200:
        sys.exit('Failure: ' + str(response.status_code) + ' - ' + response.reason)
    return response.json()

## Request Query
def request_query(url=None, query=None, token=None):    
    attempt = 0 
    success = False

    req_header = {
        'Authorization': token['token_type'] + ' ' + token['access_token']
    }

    if query == None:
        query = {
            #"interval": utilidades.obtener_fecha(),
            "interval": intervalo,
            "order": config['query']['order'],
            "orderBy": config['query']['orderBy'],
            "paging": {
                "pageSize": config['query']['page_size'],
                "pageNumber": config['query']['page_number']
            }
        }

    if url == None:
        url = "https://api.mypurecloud.com/api/v2/analytics/conversations/details/query"

    while(not success):
        attempt += 1
        response = requests.post(url, json=query, headers=req_header)
        #api rate limit or service unavailable
        if(response.status_code >= 429):
            time.sleep(60) #wait 60 seconds to retry
            if(attempt == maxAttempts):
                sys.exit('Failure: ' + response.reason + ' - ' + str(response.json()))
        else:
            if response.status_code != 200:
                sys.exit('Failure: ' + str(response.status_code) + ' - ' + response.reason)
            else:
                success = True

    return response.json()

## Request Query
def request_query_get(url=None, token=None, entrada={}):
    attempt = 0 
    success = False

    req_header = {
        'Authorization': token['token_type'] + ' ' + token['access_token']
    }

    if url == None:
        url = "https://api.mypurecloud.com/api/v2/routing/queues"


    while(not success):
        attempt += 1
        response = requests.get(url, headers=req_header)

        if response.status_code == 404:
            return entrada
        
        #api rate limit or service unavailable
        elif(response.status_code >= 429):                      
            time.sleep(60) #wait 60 seconds to retry again
            if(attempt == maxAttempts):
                sys.exit('Failure: ' + response.reason + ' - ' + str(response.json()))
        else:
            if response.status_code != 200:
                sys.exit('Failure: ' + str(response.status_code) + ' - ' + response.reason)
            else:
                success = True

    return response.json()


## REQUEST QUERY - SUMARIZADO COLAS ####
def request_query_sumarizado_colas(token):
    url = "https://api.mypurecloud.com/api/v2/analytics/conversations/aggregates/query"
    body = {
        #"interval": utilidades.obtener_fecha(),
        "interval": intervalo,
        "granularity": 'PT'+config['query_queue']['granularity']+'M',
        "groupBy": [
            "queueId"
        ],
        "filter": {
            "type": "or",
            "predicates": [
            {
                "type": "dimension",
                "dimension": "direction",
                "operator": "matches",
                "value": "inbound"
            }
            ]
        },
        "views": [
        {
            "name": "tAbandon10",
            "target": "tAbandon",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "10000"
            }
        },
        {
            "name": "tAbandon15",
            "target": "tAbandon",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "15000"
            }
        },
        {
            "name": "tAbandon20",
            "target": "tAbandon",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "20000"
            }
        },
        {
            "name": "tAbandon30",
            "target": "tAbandon",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "30000"
            }
        },
        {
            "name": "tAnswered10",
            "target": "tAnswered",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "10000"
            }
        },
        {
            "name": "tAnswered15",
            "target": "tAnswered",
            "function": "rangeBound",
            "range": {
                "gte": 0,
                "lt": "15000"
            }
        },
        {
        "name": "tAnswered20",
        "target": "tAnswered",
        "function": "rangeBound",
        "range": {
            "gte": 0,
            "lt": "20000"
        }
        },
        {
        "name": "tAnswered30",
        "target": "tAnswered",
        "function": "rangeBound",
        "range": {
            "gte": 0,
            "lt": "30000"
        }
        }
        ]
    }
    return request_query(url, body, token)

## REQUEST QUERY - DETALLE LLAMADA ###
#def request_query_dtllamada():
def request_query_dtllamada(pageNumber, token):
    url = "https://api.mypurecloud.com/api/v2/analytics/conversations/details/query"
    body = {
        #"interval": utilidades.obtener_fecha(),
        "interval": intervalo,
        "order": config['query']['order'],
        "orderBy": config['query']['orderBy'],
        #"paging": {"pageSize": config['query']['page_size'], "pageNumber": config['query']['page_number']}
        "paging": {"pageSize": config['query']['page_size'], "pageNumber": pageNumber}
    }

    return request_query(url, body, token)

## REQUEST QUERY - SUMARIZADO AGENTES ####
def request_query_sumarizado_puestos(token=None):
    url = "https://api.mypurecloud.com/api/v2/analytics/conversations/aggregates/query"
    body = {
        #"interval": utilidades.obtener_fecha(),
        "interval": intervalo,
        "granularity": 'PT'+config['query_queue']['granularity']+'M',
        "groupBy": [
            "userId",
            "queueId"
        ],
        "views": []
    }
    return request_query(url, body, token)

## REQUEST QUERY - SUMARIZADO ESTATUS AGENTES ####
def request_query_sumarizado_estatus_agente(token, usuarios):
    filter = []
    #agregar los id de usuario al filtro
    for usuario in usuarios:
        filter.append({
            "type": "dimension",
            "dimension": "userId",
            "operator": "matches",
            "value": usuario
        })
       
   
    url = "https://api.mypurecloud.com/api/v2/analytics/users/aggregates/query"
    body = {
        "interval": intervalo,
        "granularity": 'PT'+config['query_queue']['granularity']+'M',
        "groupBy": [
        "userId"
        ],        
        "filter": {
        "type": "or",
        "predicates": filter
        },        
        "metrics": [
        "tSystemPresence"
        ]
        }
    return request_query(url, body, token)

## REQUEST QUERY - CATALOGO COLAS ###
def request_query_cola(pageNumber, token, entrada={}):
    if pageNumber != 0:
        #url = "https://api.mypurecloud.com/api/v2/routing/queues&pageNumber="+pageNumber
        url = "https://api.mypurecloud.com/api/v2/routing/queues?pageNumber="+str(pageNumber)
    else:
        url = "https://api.mypurecloud.com/api/v2/routing/queues"
    
    return request_query_get(url, token, entrada={})

## REQUEST QUERY - CATALOGO USUARIOS ###
def request_query_usuarios(pageNumber, token, entrada={}):
    url = "https://api.mypurecloud.com/api/v2/users?pageNumber="+str(pageNumber)+"&pageSize=500&state=active%2Cinactive%2Cdeleted"
    return request_query_get(url, token, entrada)

## REQUEST QUERY - CATALOGO SUPERVISOR AGENTE ###
def request_query_grupo_supervisor(pageNumber, token, entrada={}):
    url = "https://api.mypurecloud.com/api/v2/groups?pageNumber="+str(pageNumber)
    return request_query_get(url, token, entrada)

def request_query_supervisores(groupId ,pageNumber, token, entrada={}):
    url = "https://api.mypurecloud.com/api/v2/groups/"+groupId+"/members?pageNumber="+str(pageNumber)
    return request_query_get(url, token, entrada)

def request_query_agente_supervisor(supervisorId, token, entrada={}):
    url = "https://api.mypurecloud.com/api/v2/users/"+supervisorId+"/directreports"
    return request_query_get(url, token, entrada)


## FUNCICION PARA SUMARIZADO COLAS
def parse_metric(metric, arr, default):
    item = jmespath.search("metrics[?metric=='" + metric + "'].stats", arr)
    return item[0] if item else default

def parse_views(view, arr, default):
    item = jmespath.search("views[?name=='" + view + "'].stats", arr)
    return item[0] if item else default

#def parse_field(value):
#    valor = str(value).replace(',', '')
#    return valor

#Manejo de Ficheros, eliminar si existe previamente. Debe mejorar a dejar evidencia
def erasefile(filename):
    if os.path.exists(filename): os.remove(filename)
    return ''

#FUNCIONES PARA GRABAR CSV EN FICHEROS
def escribir_detallellamdaconversacion(query, filename):
    with open(filename, 'a') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter='|')

        for conv in query['conversations']:
            writer.writerow([
                conv.get('conversationId'),
                conv.get('conversationStart'),
                conv.get('conversationEnd')
            ])
    return ''

def escribir_detallellamdaparticipants(query, filename, token):
    with open(filename, 'a') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter='|')

        for conv in query['conversations']:
            # Get clientCode
            conversation = conv.get('conversationId')
            #clientCode = obtener_codigo_cliente(conversation, token)
            clientCode = 'None'

            for part in conv['participants']:
                #participantName = parse_field(part.get('participantName'))

                writer.writerow([
                    str(conv.get('conversationId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                    str(part.get('participantId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                    #participantName,
                    str(part.get('participantName')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').replace(',','').rstrip('\r\n'),
                    str(part.get('userId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                    str(part.get('purpose')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                    str(part.get('externalContactId','null')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                    str(part.get('externalOrganizationId','null')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                    str(part.get('flaggedReason','null')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                    clientCode
                ])
    return ''

def escribir_detallellamadasessions(query, filename):
    with open(filename, 'a') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter='|')

        for conv in query['conversations']:
            for part in conv['participants']:
                for sess in part['sessions']:
                    #remoteNameDisplayable = str(sess.get('remoteNameDisplayable')).replace(',', '')
                    callbackscheduledTime = utilidades.obtener_fecha_default()
                    if sess.get('callbackscheduledTime'):
                        callbackscheduledTime = str(sess.get('callbackscheduledTime')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n')
                    
                    writer.writerow([
                        str(part.get('participantId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('sessionId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('mediaType')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('addressOther')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('addressSelf')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('addressFrom')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('addressTo')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('messageType')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('ani')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('direction')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('sessionDnis')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('outboundCampaignId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('dispositionAnalyzer')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('dispositionName')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('edgeId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        #remoteNameDisplayable,
                        str(sess.get('remoteNameDisplayable')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').replace(',','').rstrip('\r\n'),
                        str(sess.get('roomId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('monitoredsessionId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('monitoredparticipantId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('callbackuserName')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('callbackNumbers')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        callbackscheduledTime,
                        str(sess.get('scriptId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('peerId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('sKipEnable')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('timeoutSeconds')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('cobrowseRole')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('cobrowseroomId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('mediabridgeId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('screenshareaddressSelf')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('sharingScreen')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('screenshareroomId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('videoroomId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                        str(sess.get('videoaddressSelf')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n')
                    ])

    return ''

def escribir_detallellamadametrics(query, filename):
    with open(filename, 'a') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter='|')

        for conv in query['conversations']:
            for part in conv['participants']:
                for sess in part['sessions']:
                    sessionId = sess.get('sessionId')
                    if sess.get('metrics'):
                        #met = sess.get('metrics')
                        for met in sess.get('metrics'):
                            metric_name = met['name']
                            metric_value = met['value']
                            metric_emitDate = met['emitDate']

                            writer.writerow([
                            sessionId,
                            metric_name,
                            metric_value,
                            metric_emitDate
                        ])
    return ''

def escribir_detallellamadasegments(query, filename, token):
    with open(filename, 'a') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter='|')

        for conv in query['conversations']:
            for part in conv['participants']:
                for sess in part['sessions']:
                    for segm in sess['segments']:
                        if segm.get('queueId'): 
                            queueId = str(segm.get('queueId') ).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n')
                        else: 
                            queueId = ''
                        
                        if segm.get('errorCode'): 
                            errorCode = str(segm.get('errorCode')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n')
                        else: 
                            errorCode = ''

                        #WrapUp codeName
                        wrapUpCode = str(segm.get('wrapUpCode')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n');

                        #if wrapUpCode: 
                        #    wrapUpCodeName = obtener_codigo_finalizacion(wrapUpCode, token)
                        #else: 
                        #    wrapUpCodeName = 'None'
                        
                        wrapUpCodeName = 'None'

                        #wrapUpNote
                        wrapUpNote = str(segm.get('wrapUpNote')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n');
                        
                        wrapUpNoteCode = wrapUpNote.split('!',2);

                        if wrapUpNoteCode[0] == 'None':
                           wrapUpNoteCode = wrapUpNoteCode[0]
                        
                        else:
                           wrapUpNoteCode = wrapUpNoteCode[0] +'!'
                        
                        ##Cristina
                        ##wrapUpNote = 'None'

                        writer.writerow([
                            str(sess.get('sessionId')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                            segm.get('segmentStart'),
                            segm.get('segmentEnd'),
                            str(segm.get('segmentType')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                            queueId,
                            str(segm.get('disconnectType')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                            wrapUpCode,
                            wrapUpNoteCode,
                            errorCode.encode('utf-8', 'ignore').decode('utf-8').rstrip(),
                            wrapUpCodeName
                        ])
    return ''

def escribir_catalogocolas(query, filename):
    with open(filename, 'a') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter='|')

        for entity in query['entities']:                
            writer.writerow([
                str(entity.get('id')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                str(entity.get('name')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n')
            ])

    return ''

def escribir_catalogousuarios(query, filename):
    with open(filename, 'a', encoding='utf8') as file:
        ## Abres archivo
        writer = csv.writer(file, delimiter='|')

        for entity in query['entities']:                
            writer.writerow([
                str(entity.get('id')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                str(entity.get('name')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').replace(',',' ').rstrip('\r\n'),
                str(entity.get('email')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n'),
                str(entity.get('title')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').replace('"','').rstrip('\r\n'),
                str(entity.get('department')).encode('utf-8').decode('utf-8').replace('\n', ' ').replace(';',' ').replace('"','').rstrip('\r\n'),
                str(entity.get('state')).encode('utf-8', 'ignore').decode('utf-8').replace('\n', ' ').rstrip('\r\n')
            ])

    return ''

def buscar_grupo_supervisor(query):
    respuesta = ''
    for entity in query['entities']:
        if('SUPERVISORES' in str(entity.get('name')).upper()):
            respuesta = entity.get('id')
    
    return respuesta

def buscar_supervisor(query):
    respuesta = []
    i=0
    for entity in query['entities']:
        respuesta.insert(i, entity.get('id'))
        i = i+1
    
    return respuesta

def obtener_codigo_cliente(conversationId, token, entrada={}):
    details = request_query_get('https://api.mypurecloud.com/api/v2/conversations/' + conversationId, token, entrada)
    clientCode = 'null'
    for x in details['participants']:
        if x['purpose'] == 'customer' and x['calls'][0]['direction'] == 'inbound':
            if x.get('attributes') and x['attributes'].get('ClientCode')!=None:
                clientCode = str(x['attributes']['ClientCode'])
                break
    
    return clientCode

def obtener_codigo_finalizacion(wrapUpCode, token):
    wrapUpCodeName = 'None'
    cuerpo = {
        "name": wrapUpCode
    }
    if wrapUpCode:
        details = request_query_get('https://api.mypurecloud.com/api/v2/routing/wrapupcodes/'+wrapUpCode, token, cuerpo)
        wrapUpCodeName = details['name']
    
    return wrapUpCodeName

def is_not_empty(any_structure):
    if any_structure:
        return True
    else:
        #print('Estructura es vacia. '+str(any_structure))
        return False