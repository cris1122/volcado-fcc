import purecloud, os, csv, jmespath, json

config = purecloud.load_config()

## Define Directory
dir_name = config['output']['output_dir']

if os.path.isdir(dir_name) == False:
    os.mkdir(dir_name)

filename6 = dir_name + '/catalogocolas.csv'
filename7 = dir_name + '/catalogousuarios.csv'

purecloud.erasefile(filename6)
purecloud.erasefile(filename7)

##
## CATALOGO COLAS ###
indc = 0
query = purecloud.request_query_cola(indc)
purecloud.escribir_catalogocolas(query, filename6)

if ( query['pageCount'] > 1 ):
    while (len(query['entities']) > 0):
        indc = indc +1
        query = purecloud.request_query_cola(indc)
        if(len(query['entities']) > 0):
            purecloud.escribir_catalogocolas(query, filename6)
        
print("File catalogocolas.csv ok!")

##
## CATALOGO USUARIOS ###
indc = 1
query = purecloud.request_query_usuarios(indc)
purecloud.escribir_catalogousuarios(query, filename7)

if ( query['pageCount'] > 1 ):
    while (len(query['entities']) > 0):
        indc = indc +1
        query = purecloud.request_query_usuarios(indc)
        if(len(query['entities']) > 0):
            purecloud.escribir_catalogousuarios(query, filename7)
        
print("File catalogousuarios.csv ok!")

##
## CATALOGO SUPERVISOR AGENTE ###
#grupo Supervisor
indc = 1
query = purecloud.request_query_grupo_supervisor(indc)
groupId = purecloud.buscar_grupo_supervisor(query) 

if ( query['pageCount'] > 1 and groupId == ''):
    while (len(query['entities']) > 0):
        indc = indc +1
        query = purecloud.request_query_grupo_supervisor(indc)
        groupId = purecloud.buscar_grupo_supervisor(query)

#Supervisores
indc = 1
query = purecloud.request_query_supervisores(groupId, indc)
lista = purecloud.buscar_supervisor(query)

if ( query['pageCount'] > 1 ):
    while (len(query['entities']) > 0):
        indc = indc +1
        query = purecloud.request_query_supervisores(groupId, indc)
        lista2 = purecloud.buscar_supervisor(query)
        lista.extend(lista2)

#agentes por supervisor
for item in lista:
    query = purecloud.request_query_agente_supervisor(item)
    num = range(len(query))
    for x in num:
        with open(dir_name + '/supervisoragente.csv', 'a') as file:
            ## Abres archivo
            writer = csv.writer(file, delimiter=';')
            writer.writerow([
                query[x]['id'],
                item        
            ])

print("File supervisoragente.csv ok!")