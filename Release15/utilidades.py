import datetime

def obtener_fecha_default():
    interval='1900-01-01T05:00:00.000Z'
    return interval

def obtener_intervalo():
    now = datetime.datetime.now()
    now2 = datetime.datetime.now().minute

    #print(now)
    #print(now2)

    if now2 < 15:
        newdate = now.replace(minute=00, second=00)
        #print(newdate)
    else:
        if now2 < 30:
            newdate = now.replace(minute=15, second=00)
            #print(newdate)
        else:
            if now2 < 45:
                newdate = now.replace(minute=30, second=00)
                #print(newdate)
            else:
                if now2 <= 59:
                    newdate = now.replace(minute=45, second=00)
                    #print(newdate)

    # formatted string version
    minutes15 = (newdate + datetime.timedelta(minutes=-15))
    #print(minutes15)
    #print (newdate.strftime('%Y-%m-%dT%H:%M:')+'00.000Z')
    #print (minutes15.strftime('%Y-%m-%dT%H:%M:')+'00.000Z')

    newdate = newdate + datetime.timedelta(hours=+5)
    minutes15 = minutes15 + datetime.timedelta(hours=+5)

    intervalo = minutes15.strftime('%Y-%m-%dT%H:%M:%S')+'.000Z'+'/'+newdate.strftime('%Y-%m-%dT%H:%M:%S')+'.000Z'
    print(intervalo)
    return intervalo

def obtener_fecha():
    today = datetime.datetime.today() 
    ahora = today.strftime('%Y-%m-%d')
    yesterday = (today + datetime.timedelta(days=-1))
    ayer = yesterday.strftime('%Y-%m-%d')
    interval=ayer+'T05:00:00.000Z/'+ahora+'T05:00:00.000Z'
    return interval
    