#!/bin/bash
echo "Carga masiva...."
#sqlldr="/usr/bin/sqlldr"
CONTROL="/output/load"
DATA="/output/data"
LOG="/output/log"
BAD="/output/bad"
ROWS="50000"

#/usr/lib/sqlldr gfccpe1/gfccpe1@118.180.35.145/tst12c CONTROL=/opt/purecloud/carga.ctl, DATA=/opt/purecloud/convfinal.txt ,LOG=/opt/purecloud/log1.log  ,BAD=/opt/purecloud/bad.log ,ROWS=5000000

#carga.ctl = tabla
#convfinal.txt = archivo CSV
#log1 = log del query

#DETELLELLAMADA
#$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/catalogocolas.ctl DATA=$DATA/catalogocolas.csv  LOG=$LOG/log_catalogocolas.txt BAD=$BAD/bad_catalogocolas.log ERRORS=4000 bindsize=20000000 readsize=20000000
#$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/catalogousuarios.ctl DATA=$DATA/catalogousuarios.csv  LOG=$LOG/log_catalogousuarios.txt BAD=$BAD/bad_catalogousuarios.log ERRORS=4000 bindsize=20000000 readsize=20000000
#$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/supervisoragente.ctl DATA=$DATA/supervisoragente.csv  LOG=$LOG/log_supervisoragente.txt BAD=$BAD/bad_supervisoragente.log ERRORS=4000 bindsize=20000000 readsize=20000000

#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@10.17.202.209:1521/ORCLPDB1 CONTROL=/home/catalogocolas.ctl DATA=/home/catalogocolas.csv LOG=/home/log_catalogocolas.txt BAD=/home/bad_catalogocolas.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@192.168.1.12:1521/ORCLPDB1 CONTROL=/home/catalogousuarios.ctl DATA=/home/catalogousuarios.csv LOG=/home/log_catalogousuarios.txt BAD=/home/bad_catalogousuarios.log ERRORS=4000
#docker run --rm -v ~/channels/contact-center/output/prueba:/home oracle/instantclient:12.2.0.1 sqlldr CONCEN/password@10.17.202.209:1521/ORCLPDB1 CONTROL=/home/supervisoragente.ctl DATA=/home/supervisoragente.csv LOG=/home/log_supervisoragente.txt BAD=/home/bad_supervisoragente.log ERRORS=4000

###
#$sqlldr CONCEN/tiqWSNyRQBgEYph1@118.180.35.145/tst12c CONTROL=$CONTROL/sumarypuesto.ctl DATA=$DATA/sumarypuestos.csv  LOG=$LOG/log_ BAD=$BAD/bad_puestos.log

#sqlldr CONCEN/password@10.17.202.209:1521/ORCLPDB1 CONTROL=cargadtconv.ctl DATA=detallellamadaconversacion.csv LOG=log_dtconversa.txt BAD=bad_convfinal.log ERRORS=4000
